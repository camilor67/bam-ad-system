function countdown(item, date_reain) {
  setTimeout(function () {
    var currentDate = new Date();
    var diff = Math.abs(date_reain - currentDate);
    if (date_reain < currentDate) {
      diff = 0;
    }
    msToTime(item,diff);
    countdown(item, date_reain);
  }, 1000);
}

function msToTime(item, duration) {
  var seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours = Math.floor((duration / (1000 * 60 * 60)) % 24),
    days = Math.floor((duration / (1000 * 60 * 60 * 24)));

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  var dayCount = document.getElementById("bam-ad-rt-days"+item);
  dayCount.innerHTML = pad(days, 2);

  var hourCount = document.getElementById("bam-ad-rt-hours"+item);
  hourCount.innerHTML = pad(hours, 2);

  var minCount = document.getElementById("bam-ad-rt-min"+item);
  minCount.innerHTML = pad(minutes, 2);

  var secCount = document.getElementById("bam-ad-rt-sec"+item);
  secCount.innerHTML = pad(seconds, 2);

  return days + ' ' + hours + ":" + minutes + ":" + seconds;
}

function pad(num, size) {
  var s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}

window.onresize = doALoadOfStuff;
doALoadOfStuff();

function doALoadOfStuff() {
  var adwidth = jQuery('.bam-ad-container').width();
  var wwidth = jQuery(window).width();
  if (adwidth < 695 && wwidth > 710) {
    jQuery('.bam-ad-image').addClass("bam-ad-image-js1");
    jQuery(".bam-ad-content").addClass("bam-ad-content-js1");
    jQuery('.bam-ad-button-content').addClass("bam-ad-button-content-js1");
  } else {
    jQuery('.bam-ad-image').removeClass("bam-ad-image-js1");
    jQuery(".bam-ad-content").removeClass("bam-ad-content-js1");
    jQuery('.bam-ad-button-content').removeClass("bam-ad-button-content-js1");
  }

  if (adwidth < 596 && wwidth > 710) {
    jQuery('.bam-ad-image').addClass("bam-ad-image-js2");
    jQuery(".bam-ad-content").addClass("bam-ad-content-js2");
    jQuery('.bam-ad-button-content').addClass("bam-ad-button-content-js2");
    jQuery('.bam-ad-container').addClass("bam-ad-container-js2");
    jQuery('.bam-ad-rt-nx').addClass("bam-ad-rt-nx-js2");
    jQuery('.bam-ad-rt-content').addClass("bam-ad-rt-content-js2");
    jQuery('.bam-ad-content-tx').addClass("bam-ad-content-tx-js2");
  } else {
    jQuery('.bam-ad-image').removeClass("bam-ad-image-js2");
    jQuery(".bam-ad-content").removeClass("bam-ad-content-js2");
    jQuery('.bam-ad-button-content').removeClass("bam-ad-button-content-js2");
    jQuery('.bam-ad-container').removeClass("bam-ad-container-js2");
    jQuery('.bam-ad-rt-nx').removeClass("bam-ad-rt-nx-js2");
    jQuery('.bam-ad-rt-content').removeClass("bam-ad-rt-content-js2");
    jQuery('.bam-ad-content-tx').removeClass("bam-ad-content-tx-js2");
  }
}