<?php
/*
 * Plugin Name: Ad system - BAM TEST
 * Description: Plugin to create, edit and delete ads
 * Author: Camilo Rodriguez Sanchez
 * Version: 1.0
 */

require '__auto_load.php';
add_action('init', 'RegisterAdType');

function RegisterAdType()
{
  register_post_type(
    'bam-ad',
    array(
      'labels' => array(
        'name' => __('BAM Ads'),
        'singular_name' => __('BAM ad'),
        'featured_image'        => __('Ad Image'),
        'set_featured_image'    => __('Set ad image'),
        'remove_featured_image' => __('Remove ad image'),
        'use_featured_image'    => __('Use as ad image'),
      ),
      'public' => false,
      'show_ui' => true,
      'supports' => array('title', 'thumbnail'),
      'register_meta_box_cb' => 'AdCustomFields',
      'menu_icon' => PLUGIN_BAM_AD_URL . '/img/ad-icon.svg',
    )
  );
}

add_action('edit_form_after_title', 'myprefix_edit_form_after_title');

function myprefix_edit_form_after_title($post)
{
  if ($post->post_type == 'bam-ad') {
    echo '<h2>Shortcode</h2>';
    echo "<input type='text' value='[bam_ad id=" . $post->ID . "]'>";
  }
}

function AdCustomFields()
{
  add_meta_box('ad-type', 'Ad type', 'AdType', 'bam-ad', 'normal', 'default');
  add_meta_box('ad-metabox-texts', 'Ad texts', 'ad_texts', 'bam-ad', 'normal', 'low');
  add_meta_box('ad-metabox-color', 'Default bg color', 'myadpluging_meta_box', 'bam-ad', 'normal', 'low');
  add_meta_box('ad-metabox-remaining', 'Remaining time', 'remaining_time', 'bam-ad', 'normal', 'low');
}

function AdType()
{
  global $post;
  $type = get_post_meta($post->ID, 'adtype', true);
  ob_start(); ?>
  <div class="post-type-archive-project">
    <ul>
      <li>
        <input type="radio" name="ad-type-pick" id="ad-type-pick" value="pick" <?php echo ($type == "pick" || $type == "") ? "checked" : ""; ?>>
        <label for="ad-type-pick">Pick</label>
      </li>
    </ul>
  </div>
  <?php
  $content = ob_get_contents();
  ob_end_clean();
  echo $content;
}

function ad_texts($post)
{
  $custom = get_post_custom($post->ID);
  $ad_remaining_time_text = (isset($custom['ad_remaining_time_text'][0])) ? $custom['ad_remaining_time_text'][0] : '';
  $ad_pick_text = (isset($custom['ad_pick_text'][0])) ? $custom['ad_pick_text'][0] : '';
  $ad_hurry_up_text = (isset($custom['ad_hurry_up_text'][0])) ? $custom['ad_hurry_up_text'][0] : '';
  $ad_trusted_text = (isset($custom['ad_trusted_text'][0])) ? $custom['ad_trusted_text'][0] : '';
  ?>
  <div class="pagebox">
    <table>
      <tr>
        <td>
          <label for="ad_remaining_time_text"><?php esc_attr_e('Ingress text to display for "remaining time msg": ', 'mytheme'); ?></label>
        </td>
        <td>
          <input type="text" name="ad_remaining_time_text" id="ad_remaining_time_text" value="<?php esc_attr_e($ad_remaining_time_text); ?>" />
        </td>
      </tr>
      <tr>
        <td>
          <label for="ad_pick_text"><?php esc_attr_e('Ingress text to display for "our pick": ', 'mytheme'); ?></label>
        </td>
        <td>
          <input type="text" name="ad_pick_text" id="ad_pick_text" value="<?php esc_attr_e($ad_pick_text); ?>" />
        </td>
      </tr>
      <tr>
        <td>
          <label for="ad_hurry_up_text"><?php esc_attr_e('Ingress text to display for "hurry up"', 'mytheme'); ?></label>
        </td>
        <td>
          <input type="text" name="ad_hurry_up_text" id="ad_hurry_up_text" value="<?php esc_attr_e($ad_hurry_up_text); ?>" />
        </td>
      </tr>
      <tr>
        <td>
          <label for="ad_trusted_text"><?php esc_attr_e('Ingress text to display for "Trusted"', 'mytheme'); ?></label>
        </td>
        <td>
          <input type="text" name="ad_trusted_text" id="ad_trusted_text" value="<?php esc_attr_e($ad_trusted_text); ?>" />
        </td>
      </tr>
    </table>
  </div>
<?php
}

function remaining_time($post)
{
  $custom = get_post_custom($post->ID);
  $ad_remaining_time = (isset($custom['ad_remaining_time'][0])) ? $custom['ad_remaining_time'][0] : '';
  ?>
  <div class="pagebox">
    <p><?php esc_attr_e('Choose remaining time.', 'mytheme'); ?></p>
    <input type="datetime-local" name="ad_remaining_time" id="ad_remaining_time" value="<?php esc_attr_e($ad_remaining_time); ?>" />
  </div>
<?php
}

add_action('wp_enqueue_scripts', 'adplugin_backend_scripts', 20);

if (!function_exists('adplugin_backend_scripts')) {
  function adplugin_backend_scripts($hook)
  {
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('wp-color-picker');
    wp_enqueue_script('pickAdtype', PLUGIN_BAM_AD_URL . '/js/pick.js', array('jquery'), @filemtime(PLUGIN_BAM_AD_URL . '/js/pick.js'), true);
    wp_register_style('pickAdtypecss', PLUGIN_BAM_AD_URL . '/css/ad-pick.css', false, '1.0', 'all');
    wp_enqueue_style('pickAdtypecss');
  }
}

if (!function_exists('myadpluging_meta_box')) {
  function myadpluging_meta_box($post)
  {
    $custom = get_post_custom($post->ID);
    $default_ad_color = (isset($custom['default_ad_color'][0])) ? $custom['default_ad_color'][0] : '';
    ?>
    <script>
      jQuery(document).ready(function($) {
        $('.color_field').each(function() {
          $(this).wpColorPicker();
        });
      });
    </script>
    <div class="pagebox">
      <p><?php esc_attr_e('Select default color.', 'mytheme'); ?></p>
      <input class="color_field" type="text" name="default_ad_color" value="<?php esc_attr_e($default_ad_color); ?>" />
    </div>
  <?php
}
}

add_shortcode('bam_ad', 'render_bam_ad');

function render_bam_ad($atts)
{
  static $count = 0;
  $count++;

  $attsIn = shortcode_atts(array(
    'id' => ""
  ), $atts);

  $post_id = get_the_ID();

  $custom = get_post_custom($attsIn['id']);

  ob_start();
  if ($custom["adtype"][0] == "pick") {
    include('template/tpl-pick.php');
  } else if ($custom["adtype"][0] == "rich") { }
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}

add_action('save_post', 'saveBAMAdMetadaFields', 10, 2);

function saveBAMAdMetadaFields($post_id, $post)
{
  if ($post->post_type == 'bam-ad') {
    if (isset($_POST['ad-type-pick'])) {
      update_post_meta($post_id, 'adtype', $_POST['ad-type-pick']);
      if (isset($_POST['default_ad_color'])) {
        update_post_meta($post_id, 'default_ad_color', $_POST['default_ad_color']);
      }
      if (isset($_POST['ad_remaining_time'])) {
        update_post_meta($post_id, 'ad_remaining_time', $_POST['ad_remaining_time']);
      }
      if (isset($_POST['ad_remaining_time_text'])) {
        update_post_meta($post_id, 'ad_remaining_time_text', $_POST['ad_remaining_time_text']);
      }
      if (isset($_POST['ad_pick_text'])) {
        update_post_meta($post_id, 'ad_pick_text', $_POST['ad_pick_text']);
      }
      if (isset($_POST['ad_hurry_up_text'])) {
        update_post_meta($post_id, 'ad_hurry_up_text', $_POST['ad_hurry_up_text']);
      }
      if (isset($_POST['ad_trusted_text'])) {
        update_post_meta($post_id, 'ad_trusted_text', $_POST['ad_trusted_text']);
      }
    }
  }
}

function get_background_by_category($id, $default)
{
  $post_categories = wp_get_post_categories($id);

  foreach ($post_categories as $c) {
    $cat = get_category($c);
    if ($cat->name == "NFL") {
      return "black";
    } else if ($cat->name == "NBA") {
      return "orange";
    } else if ($cat->name == "MLB") {
      return "blue";
    } else {
      return $default;
    }
  }
}

function shortcode_column($columns)
{
  $new_columns = array(
    'shortcode' => __('Shortcode'),
  );
  return array_merge($columns, $new_columns);
}
add_filter('manage_bam-ad_posts_columns', 'shortcode_column');

add_action('manage_posts_custom_column', 'custom_columns');

function custom_columns($column)
{
  global $post;

  switch ($column) {
    case 'shortcode':
      echo "[bam_ad id=" . $post->ID . "]";
      break;
  }
}

?>