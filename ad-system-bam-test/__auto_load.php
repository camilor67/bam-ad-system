<?php
if ( ! defined( 'PLUGIN_BAM_AD_URL' ) )
  define( 'PLUGIN_BAM_AD_URL', plugins_url( '', __FILE__ ) );

if ( ! defined( 'PLUGIN_BAM_AD_FILE' ) )
  define( 'PLUGIN_BAM_AD_FILE', plugin_dir_path( __FILE__ ) );