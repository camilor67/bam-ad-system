<div class="bam-ad-container" style="background-color:<?php echo get_background_by_category($post_id, $custom["default_ad_color"][0]); ?>">
  <div class="bam-ad-col bam-ad-image">
    <?php if (has_post_thumbnail($attsIn['id'])) {
      echo get_the_post_thumbnail($attsIn['id'], 'large');
    } ?>
  </div>
  <div class="bam-ad-col bam-ad-content">
    <div class="bam-ad-content-rt-tx">
      <div class="bam-ad-rt-content">
        <div class="bam-ad-rt-item">
          <div class="bam-ad-rt-title">
            <?php echo __("DAYS") ?>
          </div>
          <div class="bam-ad-rt-number" id="bam-ad-rt-days<?php echo $count; ?>">
            00
          </div>
        </div>
        <div class="bam-ad-rt-item">
          <div class="bam-ad-rt-title">
            <?php echo __("HOURS") ?>
          </div>
          <div class="bam-ad-rt-number" id="bam-ad-rt-hours<?php echo $count; ?>">
            00
          </div>
        </div>
        <div class="bam-ad-rt-item">
          <div class="bam-ad-rt-title">
            <?php echo __("MIN") ?>
          </div>
          <div class="bam-ad-rt-number" id="bam-ad-rt-min<?php echo $count; ?>">
            00
          </div>
        </div>
        <div class="bam-ad-rt-item">
          <div class="bam-ad-rt-title">
            <?php echo __("SEC") ?>
          </div>
          <div class="bam-ad-rt-number" id="bam-ad-rt-sec<?php echo $count; ?>">
            00
          </div>
        </div>
      </div>
      <div class="bam-ad-rt-nx">
        <?php echo $custom["ad_remaining_time_text"][0] ?>
      </div>
    </div>
    <div class="bam-ad-content-tx">
      <h2>
        <?php echo $custom["ad_pick_text"][0]; ?>
      </h2>
      <div>
        <p>
          <?php echo $custom["ad_hurry_up_text"][0]; ?>
        </p>
      </div>

    </div>
  </div>
  <div class="bam-ad-col bam-ad-button-content">
    <button><?php echo __("BET & WIN") ?></button>
    <span class="overline"><?php echo $custom["ad_trusted_text"][0]; ?></span>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    var date_reain = new Date('<?php echo $custom["ad_remaining_time"][0] ?>');
    var currentDate = new Date();
    var diff = Math.abs(date_reain - currentDate);
    if (date_reain < currentDate) {
      diff = 0;
    } else {
      countdown(<?php echo $count; ?>, date_reain);
    }
    msToTime(<?php echo $count; ?>, diff);
  })
</script>