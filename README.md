**Plugin for Born Again Media Test**
By Camilo Rodriguez

This plugin is a basic Ad System. It creates a new custom post type 'bam-ad'.

The Ads only has one type 'Pick'. It allows to assing a baner image, time remaining and a default background color.
The background color of the ad is determined automatically by the category of the post where it’s displayed: NFL: black, NBA: orange, MLB: blue. If the post does not have any of those category it will use the background color stored as a default.

